require 'test_helper'

class EmailTest < ActiveSupport::TestCase

  test "clean_inner_body for etsy.com should keep content" do
    email = email_for_fixture('etsy_email.html')
    assert_match %r{http://www.etsy.com/images/email/meteor/logos/success.png}, email.clean_inner_body
  end

  private
  def email_for_fixture(file)
    email = Email.new(:body => File.read(Rails.root.join('test/fixtures/files/' + file)))
    email.account = build(:account)
    email
  end

end
