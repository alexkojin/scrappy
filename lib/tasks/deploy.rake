require 'net/ssh'

desc "Deploy site to production"
task :deploy do
  host        = 'playityet.com'
  user        = 'root'
  options     = {:password => 'qwe1234alexkojin'}
  remote_path = '/var/www/wiclone'
  
  # "cd #{remote_path} && sudo git fetch",
  # "cd #{remote_path} && sudo git reset --hard origin/master",
  
  commands = [
    'source "/usr/local/rvm/scripts/rvm"',
    'rvm ruby-1.9.3-p194@scrappy',
    "cd #{remote_path}",
    "git pull",
    "bundle install",
    "bundle exec rake db:migrate RAILS_ENV=production",
    "bundle exec whenever --update-crontab",
    "bundle exec rake assets:precompile",
    "restart wiclone"
    "kill -s USR2 `cat #{remote_path}/tmp/pids/unicorn.pid`"
  ]

  commands = Array.wrap(commands.join(';'))

  Net::SSH.start(host, user, options) do |ssh|
    commands.each do |c|
      ssh.exec(c) do |ch, stream, data|
        puts data
      end
    end
    ssh.loop
  end
end