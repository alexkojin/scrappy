module EmailsHelper
  def cover_image_tag(email, max_width = 200)
    if email.cover_image.present?
      width = email.cover_image_width || 200
      height = email.cover_image_height || 200
      if width > max_width
        height = height * max_width / width
        width  = max_width
      end
      
      # image_tag email.cover_image, width: width, height: height
      # Use raw html to exclude error ActionView::Template::Error ( isn't precompiled)
      raw("<img src='#{email.cover_image}' width='#{width}' height='#{height}'")
    else
      image_tag 'default-cover.jpg', width: 200, height: 200
    end
  end

  def search_link_to(title, email, options={})
    query = "site:#{email.site.slug}"
    options.reverse_merge!({'data-search' => query, class: 'search-link'})
    
    link_to title, root_path(search: query), options
  end
end
