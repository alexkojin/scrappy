class Site < ActiveRecord::Base
  has_many :emails, dependent: :delete_all
  
  attr_accessible :title, :url, :email, :slug, :approved
  validates :title, :url, :email, presence: true
  
  before_validation :assign_title_and_url, on: :create

  extend FriendlyId
  friendly_id :title, use: :slugged

  scope :not_approved, where(approved: false)

  private
  def assign_title_and_url
    splitted = self.email.split('@').last.split('.').reverse[0..1].reverse
    self.title = splitted.first
    self.url = splitted.join('.')
  end
end

# == Schema Information
#
# Table name: sites
#
#  id    :integer          not null, primary key
#  title :string(255)      not null
#  url   :string(255)      not null
#  email :string(255)      not null, indexed
#  slug  :string(255)      not null, indexed
#

