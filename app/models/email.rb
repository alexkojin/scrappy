# coding: utf-8

require 'gmail'

class Email < ActiveRecord::Base
  belongs_to :account
  belongs_to :site
  attr_accessible :from, :subject, :body, :slug

  extend FriendlyId
  friendly_id :subject, use: :slugged

  include PgSearch
  pg_search_scope :search_by_subject, :against => :subject

  validates :from, :subject, :body, :slug, presence: true

  before_create :assign_site, :assign_cover_image

  scope :latest, order('created_at DESC')
  scope :recent, lambda{|limit| order('created_at DESC').limit(limit) }

  def self.post_to_twitter
    email = Email.where("created_at > ?", 10.minutes.ago).first
    email.post_to_twitter if email
  end

  # Clean email from account's email, unsubscribe links, ...
  def clean_inner_body
    # remove account's email
    stop_email = [self.account.email, self.account.email.split('@').first].join('|')
    body = self.body.gsub(/#{stop_email}/i, '')

    html = Nokogiri::HTML(body)

    # remove all stop words links
    stop_words = %w(subscri here view link online account mobile why\ did friend browser manage preference)
    stop_words.each do |word|
      i = search_text(html, word)
      i.xpath('./ancestor::a').remove
      i.remove
    end

    # remove css styles 
    html.css('style').remove
    # link should be opened in new browser's tab
    html.css('a, area').each {|link| link['target'] = '_blank'}

    # the email can have two body tag (it's email's template error)
    html.css('body').map(&:inner_html).join('')
  end

  def post_to_twitter
    message = "##{self.site.title} #{self.subject} #{Rails.application.routes.url_helpers.email_url(self, host: 'wiclone.com')}"
    Twitter.update(message)
  end

  # Find best image for the email
  def find_cover_image
    images = []
    Nokogiri::HTML(self.body).css('img').each do |img|
      if img['src']
        size = FastImage.size(img['src']) rescue nil
        images << {src: img['src'], 
                   w: size.first,
                   h: size.last,
                   q: (size.first - size.last).abs, # difference between width and height
                   s: size.first * size.last # square
                  } if size
      end
    end
    # sort by q by asceding
    images.sort!{|a,b| a[:q] <=> b[:q]}
    # select images with square more than 10000 (100x100)
    images = images.select{|image| image[:s] > 10000 }
    images.first
  end

  def assign_cover_image
    image = self.find_cover_image
    if image
      self.cover_image = image[:src]
      self.cover_image_width = image[:w]
      self.cover_image_height = image[:h]
    end
  end

  private
  def assign_site
    self.site = Site.find_or_create_by_email(self.from)
  end

  def search_text(html, text)
    html.search("//*[contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'#{text}')]")
  end

end

# == Schema Information
#
# Table name: emails
#
#  id                 :integer          not null, primary key
#  account_id         :integer          indexed
#  site_id            :integer
#  from               :string(255)      not null
#  subject            :string(500)      not null
#  body               :text             not null
#  slug               :string(500)      not null, indexed
#  created_at         :datetime
#  cover_image        :string(255)
#  cover_image_width  :integer
#  cover_image_height :integer
#

