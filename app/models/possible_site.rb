class PossibleSite < ActiveRecord::Base
  attr_accessible :name, :category, :done, :yahoo_url, :yahoo_products_url, :rating, :ratings_count, :reviews_count
  
  validates_presence_of :name
  validates_uniqueness_of :name, uniq: true

  before_create :assign_url

  def assign_url
    self.url = self.name.downcase.strip.gsub(/\s/m, '')
    self.url << '.com' unless name =~ /\.com|\.net|\.us/
  end
end
