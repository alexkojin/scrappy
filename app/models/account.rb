class Account < ActiveRecord::Base
  has_many :emails, dependent: :nullify
  attr_accessible :email, :password

  def to_s
    self.email
  end
end

# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  email      :string(255)
#  password   :string(255)
#  created_at :datetime
#

