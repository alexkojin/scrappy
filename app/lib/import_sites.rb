module ImportSites
  extend self

  def from_yahoo
    (1..70).each do |page_id|
      puts " *** #{page_id} ***"
      puts ""
      parse_yahoo_page("http://shopping.yahoo.com/stores/page--#{page_id}/results--45")
    end
  end

  def parse_yahoo_page(url)
    page = Nokogiri::HTML(open(url).read)

    page.css('.merchants-listing li').each do |li|
      options = {
        name: li.css('.title a').text,
        yahoo_url: "http://shopping.yahoo.com" + li.css('.title a').attr('href').to_s,
        yahoo_products_url: li.css('>small a:last-child').attr('href').to_s,
        ratings_count: li.css('.merchant-rating > a').attr('title').to_s.to_i,
        reviews_count: li.css('.merchant-rating a[title="Read Reviews"]').text.to_i
      }

      rating = li.css('.merchant-rating > a').attr('class').to_s.match(/shrating_(\d_\d|\d)/mi)[1]
      options[:rating] = rating.split('_').join('.').to_f

      puts options[:name]

      PossibleSite.create(options)
    end
  end
end