module EmailDownloader
  extend self

  # Fetch latest emails from all gmail accounts
  def download
    Account.all.each do |account|
      Gmail.new(account.email, account.password) do |gmail|
        gmail.inbox.emails(:unread).each do |email|
          puts "##{email.uid} #{email.subject} (#{email.from})"
          body = email.html_part ? email.html_part.body.decoded: email.body.decoded

          account.emails.create from: email.from.first,
                             subject: email.subject[0..500],
                                body: body
          email.mark(:unread) unless Rails.env.production?
        end
      end
    end
  end

end