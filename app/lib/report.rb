module Report
  extend self

  def numbers_emails_by_day
    Email.find_by_sql("SELECT date_trunc('day', created_at) AS Day, count(*) AS total FROM emails WHERE created_at > now() - interval '1 months' GROUP BY 1 ORDER BY 1 DESC")
  end
end