class EmailsController < ApplicationController
  before_filter :authenticate_admin_user!, only: [:raw, :destroy]
  before_filter :find_email, only: [:show, :raw, :destroy]

  def index
    @emails = Email.joins(:site).where("sites.approved = true").latest
    if params[:search]
      if params[:search].starts_with?('site:')
        @emails = @emails.where("sites.slug = ?", params[:search].split(':').last)
      else
        @emails = @emails.search_by_subject(params[:search])
      end
    end
    @emails = @emails.page(params[:page]).per(50)

    render(partial: 'board_items', layout: nil) if request.xhr?
  end

  def show
    render(layout: nil) if request.xhr?
  end

  def raw
    render text: @email.body, layout: nil
  end

  def destroy
    @email = Email.find(params[:id])
    @email.destroy

    redirect_to root_path
  end

  private
  def find_email
    @email = Email.find(params[:id])
  end
end
