ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => 'Dashboard'

  content :title => proc{ I18n.t("active_admin.dashboard") } do
    columns do

      column do
        panel "Numbers Emails Per Day" do
          div do
            render 'report'   
          end
        end
      end
    end

    columns do

      column do
        panel "Not Approved Sites" do
          ul do
            Site.not_approved.map do |site|
              li link_to(site.title, admin_site_path(site))
            end
          end
        end
      end

      column do
        panel "Recent Emails" do
          ul do
            Email.recent(5).map do |email|
              li link_to(email.subject, admin_email_path(email))
            end
          end
        end
      end

    end
  end # content
end
