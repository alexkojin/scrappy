ActiveAdmin.register PossibleSite do
  menu :priority => 4

  filter :name
  filter :done, as: :check_boxes

  index do                            
    column :id                     
    column :name do |site|
      link_to site.name, [:admin, site]
    end  
    column :url do |site|
      link_to site.url, 'http://' + site.url, target: '_blank'
    end      
    column :rating
    column :ratings_count
    column :reviews_count
    column(:done) { |i| best_in_place i, :done, :type => :checkbox, :path => [:admin, i] }             
    default_actions                   
  end       
end
