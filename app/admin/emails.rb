ActiveAdmin.register Email do
  menu :priority => 2
  
  index do
    column :id
    column :site
    column :subject
    column :created_at
    default_actions
  end

  action_item do
    link_to 'Fetch Emails', fetch_admin_emails_path
  end

  collection_action :fetch do
    EmailDownloader.download
    flash[:notice] = 'Emails are fetched'
    redirect_to admin_emails_path
  end
end
