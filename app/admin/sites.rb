ActiveAdmin.register Site do
  menu :priority => 3

  show do
    attributes_table do 
      row :id
      row(:title) { |i| best_in_place i, :title, :type => :input, :path => [:admin, i] }
      row(:url) { |i| best_in_place i, :url, :type => :input, :path => [:admin, i] }
      row :email
      row :slug
      row(:approved) { |i| best_in_place i, :approved, :type => :checkbox, :path => [:admin, i] }
    end

    panel("Emails (#{site.emails.count})") do
      paginated_collection(site.emails.latest.page(params[:page]).per(15), download_links: false) do
        table_for(collection, sortable: false) do
          column :id do |email|
            link_to email.id, admin_email_path(email)
          end
          column :subject do |email|
            link_to email.subject, raw_email_path(email), target: '_blank'
          end
          column :from
          column :slug
          column "Delete" do |email|
            link_to "Delete", [:admin, email], :method => :delete
          end
        end
      end
    end
  end

  action_item :only => :show do
    link_to('Aprrove & Delete emails', approve_and_delete_emails_admin_site_path(site))
  end

  member_action :approve_and_delete_emails do
    @site = Site.find(params[:id])
    @site.update_attribute(:approved, true)
    @site.emails.destroy_all

    redirect_to admin_dashboard_path
  end

end
