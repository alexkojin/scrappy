var Board = {};

Board.init = function() {

  // multi-column layout
  var wookmarkOptions = {
    container: $('#board'),
    offset: 15,
    itemWidth: 230,
    autoResize: true
  };

  var wookmarkHandler = $('.item');
  wookmarkHandler.wookmark(wookmarkOptions);

  // infinite scroll
  var footer = $('#board-footer');
  var waypointOptions = {offset: '100%', context: '#board-wrapper'};

  footer.waypoint(function(event, direction){
    footer.waypoint('remove');

    var href = footer.find('.more').attr('href');
    if(href) {
      $.get(href, function(data) {
        var $data = $(data);
        $('#board').append($data.find('.item'));
        footer.find('.more').replaceWith($data.find('.more'));

        if(wookmarkHandler) wookmarkHandler.wookmarkClear();
        wookmarkHandler = $('.item');
        wookmarkHandler.wookmark(wookmarkOptions);

        footer.waypoint(waypointOptions);
      });
    }

  }, waypointOptions);

  // ON preview mode
  $('#board-wrapper').on('click', 'a', function(){
    $('body').addClass('preview-mode');
    $('.item').wookmark(wookmarkOptions);

    // Scroll to current item
    $('#board-wrapper').scrollTop($(this).closest('.item').position().top);

    // Fetch content from server
    $.get($(this).attr('href'), function(data){
      $("#board-preview").html(data);
      $('body').scrollTop(0);
    });
    
    return false;
  });

  // OFF preview mode
  $('#board-preview').on('click', '#preview-close', function(){
    $('body').removeClass('preview-mode');
    $('.item').wookmark(wookmarkOptions);

    var top = $('#' + $(this).attr('data-item')).position().top;
    $('#board-wrapper').scrollTop(top);

    $('#board-preview').html('')

    return false;
  });
}