//= require active_admin/base
//= require vendor/highcharts
//= require best_in_place

$(document).ready(function() {
  // Activating Best In Place
  $(".best_in_place").best_in_place();
});
