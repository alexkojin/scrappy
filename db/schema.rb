# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121206074919) do

  create_table "accounts", :force => true do |t|
    t.string   "email"
    t.string   "password"
    t.datetime "created_at"
  end

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "emails", :force => true do |t|
    t.integer  "account_id"
    t.integer  "site_id"
    t.string   "from",                              :null => false
    t.string   "subject",            :limit => 500, :null => false
    t.text     "body",                              :null => false
    t.string   "slug",               :limit => 500, :null => false
    t.datetime "created_at"
    t.string   "cover_image"
    t.integer  "cover_image_width"
    t.integer  "cover_image_height"
  end

  add_index "emails", ["account_id"], :name => "index_emails_on_account_id"
  add_index "emails", ["slug"], :name => "index_emails_on_slug", :unique => true

  create_table "possible_sites", :force => true do |t|
    t.string   "name",                                  :null => false
    t.string   "url"
    t.string   "category"
    t.float    "rating"
    t.integer  "ratings_count",      :default => 0
    t.integer  "reviews_count",      :default => 0
    t.string   "yahoo_url"
    t.string   "yahoo_products_url"
    t.boolean  "done",               :default => false
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  add_index "possible_sites", ["done"], :name => "index_possible_sites_on_done"
  add_index "possible_sites", ["name"], :name => "index_possible_sites_on_name"

  create_table "sites", :force => true do |t|
    t.string  "title",                       :null => false
    t.string  "url",                         :null => false
    t.string  "email",                       :null => false
    t.string  "slug",                        :null => false
    t.boolean "approved", :default => false
  end

  add_index "sites", ["approved"], :name => "index_sites_on_approved"
  add_index "sites", ["email"], :name => "index_sites_on_email"
  add_index "sites", ["slug"], :name => "index_sites_on_slug"

end
