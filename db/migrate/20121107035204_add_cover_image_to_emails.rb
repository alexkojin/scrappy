class AddCoverImageToEmails < ActiveRecord::Migration
  def change
    add_column :emails, :cover_image, :string
  end
end
