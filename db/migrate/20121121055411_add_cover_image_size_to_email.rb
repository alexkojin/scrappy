class AddCoverImageSizeToEmail < ActiveRecord::Migration
  def change
    add_column :emails, :cover_image_width, :integer
    add_column :emails, :cover_image_height, :integer
  end
end
