class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :title, null: false
      t.string :url, null: false
      t.string :email, null: false
      t.string :slug, null: false
    end

    add_index :sites, :email, uniq: true
    add_index :sites, :slug, uniq: true
  end
end
