class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.belongs_to :account
      t.belongs_to :site
      t.string :from, null: false
      t.string :subject, null: false, limit: 500
      t.text   :body, null: false
      t.string :slug, null: false, limit: 500
      t.datetime :created_at
    end
    
    add_index :emails, :account_id
    add_index :emails, :slug, unique: true
  end
end
