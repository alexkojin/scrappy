class CreatePossibleSites < ActiveRecord::Migration
  def change
    create_table :possible_sites do |t|
      t.string :name, null: false
      t.string :url
      t.string :category
      t.float :rating
      t.integer :ratings_count, default: 0
      t.integer :reviews_count, default: 0
      t.string :yahoo_url
      t.string :yahoo_products_url
      t.boolean :done, default: false

      t.timestamps
    end

    add_index :possible_sites, :name, uniq: true
    add_index :possible_sites, :done
  end
end
