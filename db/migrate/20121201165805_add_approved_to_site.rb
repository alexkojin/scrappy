class AddApprovedToSite < ActiveRecord::Migration
  def up
    add_column :sites, :approved, :boolean, default: false
    add_index :sites, :approved

    Site.reset_column_information
    Site.update_all(approved: true)
  end

  def down
    remove_index :sites, :approved
    remove_column :sites, :approved
  end
end
