Scrappy::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  resources :emails, only: [:index, :show, :destroy], path: 'offer' do
    get 'raw', on: :member
  end
  
  root :to => 'emails#index'

end
